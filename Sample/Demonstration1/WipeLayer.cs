﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Loom;
using Loom.JoinPoints;
using RTCOS;

namespace Demonstration1
{
    // 拭き掃除モードのレイヤ
    public class WipeLayer : PartialLayer
    {
        #region コンストラクタ
        // コンストラクタ
        public WipeLayer()
            : base("Demonstration1.AppBaseLayer", 1)        // ベースレイヤ名, 優先度
        {
            // オブザーバアスペクトの指定
            ObserverAspect = new WipeObserverAspect();
        }

        #endregion

        #region オブザーバアスペクト
        // オブザーバアスペクト
        public class WipeObserverAspect : ObserverAspect
        {
            // 状態
            private enum State
            {
                NotVacuum,
                Vacuum1,
                Vacuum2,
                Vacuum3,
                VacuumMode,
                Error,
            }
            private State _State = State.NotVacuum;

            // クリティカルセクションに入る処理
            [Include(typeof(App), "Run")]
            [Include(typeof(CleanningRobot), "SensorThreadMain")]
            [Call(Advice.Around)]
            public T CriticalSection<T>([JPContext]Context ctx, params object[] args) where T : _Void
            {
                Layer.LayerActivater.EnterLayerCriticalSection("Demonstration1.AppBaseLayer");
                // ターゲットメソッドの実行
                ctx.Invoke(args);
                Layer.LayerActivater.ExitLayerCriticalSection("Demonstration1.AppBaseLayer");
                return (T)_Void.Value;
            }

            // センサ情報を確認し、レイヤの切り替えを判断する処理
            [Include(typeof(Sensor), "GetSensorData")]
            [Call(Advice.Around)]
            public T CheckSensor<T>([JPContext]Context ctx, params object[] args)
            {
                // ターゲットメソッドを実行し、センサ値を取得
                sbyte value = (sbyte)ctx.Invoke(args);
                // センサ値による状態の変化
                switch (_State)
                {
                    case State.NotVacuum:       // ゴミ無し
                        if (value == 1)
                        {
                            _State = State.Vacuum1;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.Vacuum1:         // ゴミ有り
                        if (value == 1)
                        {
                            _State = State.Vacuum2;
                        }
                        else if (value == 0)
                        {
                            _State = State.NotVacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.Vacuum2:         // ゴミ有り
                        if (value == 1)
                        {
                            _State = State.Vacuum3;
                        }
                        else if (value == 0)
                        {
                            _State = State.NotVacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.Vacuum3:         // ゴミ有り
                        if (value == 1)
                        {
                            // ゴミありが続いたので、吸引モードに移る
                            _State = State.VacuumMode;
                            // 吸引レイヤのアクティベート
                            Layer.LayerActivater.DeactivateLayer("Demonstration1.WipeLayer");
                            Layer.LayerActivater.ActivateLayer("Demonstration1.VacuumLayer");
                        }
                        else if (value == 0)
                        {
                            _State = State.NotVacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.VacuumMode:
                        if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.Error:
                        break;
                    default:
                        break;
                }
                // センサ値を返す
                return (T)((object)value);
            }
        }

        #endregion

        #region クラス群
        // アプリケーションクラス
        public class App
        {
            // 実行用メソッド (オブザーバアスペクトでウィーブするために、ベースメソッドをそのまま呼び出すメソッドを定義している)
            public virtual void Run()
            {
                Layer.LayerdObjectAccessor.Proceed();
                // ベースメソッドをそのまま実行
                //((AppBaseLayer.App)Layer.LayerdObjectAccessor.LayerdBase).Run();
            }
        }

        // 掃除ロボット
        public class CleanningRobot
        {
            // 掃除
            public virtual void Clean()
            {
                // 拭き掃除
                Console.WriteLine("***** Wipe *****");
                var sensorValue = ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._SensorValue;
                Console.WriteLine("SensorValue = " + sensorValue);
                Console.WriteLine("****************");
            }

            // センサスレッドの処理
            public virtual void SensorThreadMain()
            {
                var sensorValue = ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._Sensor.GetSensorData();
                ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._SensorValue = sensorValue;
                Thread.Sleep(500);
            }
        }

        // センサクラス ゴミを吸い込めているか?
        public class Sensor
        {
            // センサデータの取得
            public virtual sbyte GetSensorData()
            {
                return (sbyte)Layer.LayerdObjectAccessor.Proceed();
                // ベースメソッドをそのまま実行
                //return ((AppBaseLayer.Sensor)Layer.LayerdObjectAccessor.LayerdBase).GetSensorData();
            }
        }

        #endregion
    }
}
