﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS.Framework.Scheduling
{
    /// <summary>
    /// レイヤコントロールブロック。
    /// </summary>
    public class LCB
    {
        #region プロパティ
        /// <summary>
        /// レイヤ。
        /// </summary>
        public Layer Layer { get; set; }

        /// <summary>
        /// ベースレイヤ。
        /// </summary>
        public BaseLayer BaseLayer { get; set; }

        /// <summary>
        /// 次のLCB。
        /// </summary>
        public LCB NextLCB { get; set; }

        /// <summary>
        /// ID。
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// レイヤの状態。
        /// </summary>
        public LayerState LayerState { get; set; }

        /// <summary>
        /// レイヤ実行順序の優先度。
        /// </summary>
        public int Priority { get; set; }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// LCBのコンストラクタ。
        /// </summary>
        public LCB()
        {
        }

        #endregion
    }
}
