﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// パーシャルレイヤ
    /// </summary>
    public class PartialLayer : Layer
    {
        #region プロパティ
        /// <summary>
        /// ベースレイヤの名前
        /// </summary>
        public string BaseLayerName { get; protected set; }

        /// <summary>
        /// ベースレイヤ
        /// </summary>
        public BaseLayer BaseLayer { get { return Layer.LayerList.BaseLayers.Find((layer) => layer.Name == BaseLayerName); } }

        /// <summary>
        /// 優先度 (0-15)
        /// </summary>
        public byte Priority { get; protected set; }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// パーシャルレイヤ
        /// </summary>
        public PartialLayer()
            : this(DefaultBaseLayer.DefaultBaseLayerName, 8)
        {
        }

        /// <summary>
        /// パーシャルレイヤ
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤの名前</param>
        /// <param name="priority">優先度 (0-15)</param>
        public PartialLayer(string baseLayerName, byte priority)
        {
            BaseLayerName = baseLayerName;
            Priority = priority;
        }

        #endregion
    }
}
