﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤ状のオブジェクトのアクセサ。
    /// ベースオブジェクトへのアクセスや自身を参照するのに用いる。
    /// </summary>
    public abstract class ILayerdObjectAccessor
    {
        #region メソッド
        /// <summary>
        /// 重ね合わせ元のレイヤのメソッドを呼び出す。
        /// </summary>
        /// <param name="args">引数。</param>
        /// <returns>呼び出されたメソッドの戻り値。</returns>
        public abstract object Proceed(params object[] args);

        /// <summary>
        /// 自身を表すレイヤ状オブジェクトの参照。
        /// </summary>
        public abstract object LayerdThis { get; }

        /*
        /// <summary>
        /// ベースオブジェクトの参照。
        /// </summary>
        public abstract object LayerdBase { get; }*/

        #endregion
    }

    /*
    /// <summary>
    /// レイヤ状のオブジェクトのアクセサ。
    /// ベースオブジェクトへのアクセスや自身を参照するのに用いる。
    /// </summary>
    public interface ILayerdObjectAccessor
    {
        #region メソッド
        /// <summary>
        /// 重ね合わせ元のレイヤのメソッドを呼び出す。
        /// </summary>
        /// <param name="args">引数。</param>
        /// <returns>呼び出されたメソッドの戻り値。</returns>
        object Proceed(params object[] args);
        
        /// <summary>
        /// 自身を表すレイヤ状オブジェクトの参照。
        /// </summary>
        object LayerdThis { get; }
        
        /// <summary>
        /// ベースオブジェクトの参照。
        /// </summary>
        object LayerdBase { get; }

        #endregion
    }*/
}
