﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using RTCOS;

namespace DebugApplication
{
    public class LayerActivationBaseLayer : BaseLayer, ILayerEntryPoint, ILayersInitializer
    {
        #region レイヤの登録
        List<BaseLayer> _BaseLayers = new List<BaseLayer>();
        List<PartialLayer> _PartialLayers = new List<PartialLayer>();

        /// <summary>
        /// ベースレイヤ
        /// </summary>
        BaseLayer[] ILayersInitializer.BaseLayers { get { return _BaseLayers.ToArray(); } }

        /// <summary>
        /// パーシャルレイヤ
        /// </summary>
        PartialLayer[] ILayersInitializer.PartialLayers { get { return _PartialLayers.ToArray(); } }

        /// <summary>
        /// レイヤの生成
        /// </summary>
        void ILayersInitializer.CreateLayers()
        {
            _BaseLayers.Add(this);
            // 下にあるものが優先的に実行される
            _PartialLayers.Add(new LayerActivationLayer1());
            _PartialLayers.Add(new LayerActivationLayer2());
            _PartialLayers.Add(new LayerActivationLayer3());
            _PartialLayers.Add(new LayerActivationLayer4());
            _PartialLayers.Add(new LayerActivationLayer5());
        }

        #endregion

        #region ユーザプログラムのエントリポイント
        // レイヤ登録処理完了後に初めに実行される (ユーザプログラムのエントリポイント)
        void ILayerEntryPoint.LayerMain(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();                                              // ベンチマーク用ストップウォッチ
            stopwatch.Stop();
            stopwatch.Reset();
            var layerList = new string[] { "DebugApplication.LayerActivationLayer1",
                                           "DebugApplication.LayerActivationLayer2",
                                           "DebugApplication.LayerActivationLayer3",
                                           "DebugApplication.LayerActivationLayer4",
                                           "DebugApplication.LayerActivationLayer5"};
            Benchmark benchmark = (Benchmark)Layer.LayeredObjectCreater.CreateObject(typeof(Benchmark));    // フレームワークのベンチマーク
            // 計測用関数
            var benchmarkTest = new Action<int>((layernum) =>
            {
                stopwatch.Start();
                for (int i = 0; i < layernum; ++i)
                {
                    Layer.LayerActivater.ActivateLayer(layerList[i]);
                }
                Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.LayerActivationBaseLayer");
                //Layer.LayerActivater.EnterLayerCriticalSection("DebugApplication.LayerActivationBaseLayer");
                benchmark.Method0();
                benchmark.Method1();
                benchmark.Method2();
                benchmark.Method3();
                benchmark.Method4();
                //Layer.LayerActivater.ExitLayerCriticalSection("DebugApplication.LayerActivationBaseLayer");
                stopwatch.Stop();
                long time = stopwatch.ElapsedTicks;
                Console.WriteLine(layernum + "," + time);
                stopwatch.Reset();
                for (int i = 0; i < layernum; ++i)
                {
                    Layer.LayerActivater.DeactivateLayer(layerList[i]);
                }
                Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.LayerActivationBaseLayer");
            });

            // ベンチマーク開始
            Console.WriteLine("アクティベート数,実行時間," + Stopwatch.Frequency);
            // キャッシュミス
            /*
            System.Console.WriteLine("キャッシュミス");
            benchmarkTest(0);
            Layer.LayerActivater.ActivateLayer("Benchmark.LayerActivationLayer1");
            Layer.LayerActivater.WaitLayerOperationCompletion("Benchmark.LayerActivationBaseLayer");
            Layer.LayerActivater.EnterLayerCriticalSection("Benchmark.LayerActivationBaseLayer");
            benchmark.Method0();
            Layer.LayerActivater.ExitLayerCriticalSection("Benchmark.LayerActivationBaseLayer");
            Layer.LayerActivater.DeactivateLayer("Benchmark.LayerActivationLayer1");
            Layer.LayerActivater.WaitLayerOperationCompletion("Benchmark.LayerActivationBaseLayer");*/
            benchmarkTest(0);
            benchmarkTest(1);
            // キャッシュミス対策後
            System.Console.WriteLine("キャッシュミス対策後");
            benchmarkTest(0);
            benchmarkTest(1);
            benchmarkTest(2);
            benchmarkTest(3);
            benchmarkTest(4);
            benchmarkTest(5);

            // 動作確認用
            //System.Console.WriteLine("");
            //System.Console.WriteLine(benchmark.ToString());
        }

        #endregion

        #region ベンチマーク用クラス
        public class Benchmark
        {
            public int _Counter0;
            public int _Counter1;
            public int _Counter2;
            public int _Counter3;
            public int _Counter4;

            public void Method0()
            {
                ++_Counter0;
            }
            public void Method1()
            {
                ++_Counter1;
            }
            public void Method2()
            {
                ++_Counter2;
            }
            public void Method3()
            {
                ++_Counter3;
            }
            public void Method4()
            {
                ++_Counter4;
            }

            public override string ToString()
            {
                return _Counter0 + ", " + _Counter1 + ", " + _Counter2 + ", " + _Counter3 + ", " + _Counter4;
            }
        }
        #endregion
    }
}
