﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Reflection.Emit;
using Loom;

namespace RTCOS.Framework.Dispatch
{
    /// <summary>
    /// LayeredObjectの生成を行うオブジェクト。
    /// </summary>
    internal class LayeredObjectCreater : ILayeredObjectCreater
    {
        #region フィールド
        /// <summary>
        /// メッセージディスパッチャ。
        /// </summary>
        private MessageDispatcher _MessageDispatcher;

        private LayerEventManager _LayerEventManager;

        /// <summary>
        /// 
        /// </summary>
        private LayerList _LayerList;

        private Dictionary<Type, Type> _LayeredObjectTypes = new Dictionary<Type, Type>();

        #endregion

        #region コンストラクタ
        /// <summary>
        /// LayeredObjectCreaterのコンストラクタ。
        /// </summary>
        /// <param name="messageDispatcher">メッセージディスパッチャ。</param>
        public LayeredObjectCreater(MessageDispatcher messageDispatcher, LayerList layerList, LayerEventManager layerEventManager)
        {
            _MessageDispatcher = messageDispatcher;
            _LayerList = layerList;
            _LayerEventManager = layerEventManager;
        }

        #endregion

        #region メソッド
        public void Initialize()
        {
            foreach (var baseLayer in _LayerList.BaseLayers)
            {
                // アセンブリ作成
                var name = "RTCOS.Framework.LayeredObjects." + baseLayer.Name;
                AssemblyName asmName = new AssemblyName(name);
                AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.RunAndSave);
                ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(name, name + ".dll");
                // 型生成
                var baseObjectTypes = from type in baseLayer.GetType().GetNestedTypes()
                                      where !type.IsSubclassOf(typeof(ObserverAspect))
                                      select type;
                foreach (var baseObjectType in baseObjectTypes)
                {
                    // クラス作成
                    TypeBuilder typeBuilder = moduleBuilder.DefineType(name + "." + baseObjectType.Name, TypeAttributes.Public, baseObjectType, new Type[] { typeof(ILayerdObject) });
                    var createdType = CreateLayeredObjectType(typeBuilder, baseObjectType);
                    // ベースメソッドのメソッド情報を取得
                    var methods = baseObjectType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    foreach (var method in methods)
                    {
                        // 仮想関数でなければ無視
                        if (!method.Attributes.HasFlag(MethodAttributes.Virtual))
                        {
                            continue;
                        }
                        // 取得
                        _MessageDispatcher._BaseMethods[method] =
                            createdType.GetMethod("__RTCOS_Base_" + method.Name, BindingFlags.Instance | BindingFlags.NonPublic, null, (from param in method.GetParameters() select param.ParameterType).ToArray(), null);
                    }
                }
            }
        }

        private Type CreateLayeredObjectType(TypeBuilder typeBuilder, Type baseObjectType)
        {
            if (_LayeredObjectTypes.ContainsKey(baseObjectType))
            {
                return _LayeredObjectTypes[baseObjectType];
            }

            // フィールド作成
            FieldBuilder messageDispatcher = typeBuilder.DefineField("__RTCOS_MessageDispatcher_Field", typeof(MessageDispatcher), FieldAttributes.FamORAssem);
            FieldBuilder instances = typeBuilder.DefineField("__RTCOS_Instances_Field", typeof(List<object>), FieldAttributes.FamORAssem);
            // インスタンス確認用のプロパティを作成
            PropertyBuilder instancesProp = typeBuilder.DefineProperty("__RTCOS_Instances", PropertyAttributes.HasDefault, typeof(List<object>), null);
            MethodBuilder getterInstancesProp = typeBuilder.DefineMethod("get___RTCOS_Instances", MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual, typeof(List<object>), null);
            ILGenerator instancesILGenerator = getterInstancesProp.GetILGenerator();
            instancesILGenerator.Emit(OpCodes.Ldarg_0);
            instancesILGenerator.Emit(OpCodes.Ldfld, instances);
            instancesILGenerator.Emit(OpCodes.Ret);
            instancesProp.SetGetMethod(getterInstancesProp);
            var baseObjectSetter = typeof(ILayerdObject).GetMethod("get___RTCOS_Instances");
            typeBuilder.DefineMethodOverride(getterInstancesProp, baseObjectSetter);
            // コンストラクタ作成
            var constructors = baseObjectType.GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var constructor in constructors)
            {
                Type[] paramTypes = (from param in constructor.GetParameters() select param.ParameterType).ToArray();
                ConstructorBuilder constructorBuilder = typeBuilder.DefineConstructor(constructor.Attributes, CallingConventions.Standard, paramTypes);
                ILGenerator ilGenerator = constructorBuilder.GetILGenerator();
                for (int i = 0; i < (paramTypes.Length + 1); ++i)
                {
                    ilGenerator.Emit(OpCodes.Ldarg, i);
                }
                ilGenerator.Emit(OpCodes.Call, constructor);
                ilGenerator.Emit(OpCodes.Ret);
            }
            // メソッド作成
            var dispatchMethod = typeof(MessageDispatcher).GetMethod("DispatchMethod", new Type[] { typeof(object), typeof(MethodInfo), typeof(object[]) });
            var methods = baseObjectType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var method in methods)
            {
                // 仮想関数でなければ無視
                if (!method.Attributes.HasFlag(MethodAttributes.Virtual) ||
                    (method.Name == "ToString") || (method.Name == "Equals") || (method.Name == "GetHashCode") || (method.Name == "GetType") || (method.Name == "Finalize"))
                {
                    continue;
                }
                Type[] paramTypes = (from param in method.GetParameters() select param.ParameterType).ToArray();
                MethodBuilder methodBuilder = typeBuilder.DefineMethod(method.Name, method.Attributes, method.ReturnType, paramTypes);
                ILGenerator ilGenerator = methodBuilder.GetILGenerator();
                // メソッド情報取得コードの生成
                LocalBuilder methodinfo = ilGenerator.DeclareLocal(typeof(MethodInfo));
                ilGenerator.Emit(OpCodes.Ldtoken, method);
                ilGenerator.EmitCall(OpCodes.Call, typeof(MethodBase).GetMethod("GetMethodFromHandle", new Type[] { typeof(RuntimeMethodHandle) }), null);
                ilGenerator.Emit(OpCodes.Stloc, methodinfo);
                // パラメータリスト作成コードの生成
                LocalBuilder paramlists = ilGenerator.DeclareLocal(typeof(object[]));
                ilGenerator.Emit(OpCodes.Ldc_I4, paramTypes.Length);
                ilGenerator.Emit(OpCodes.Newarr, typeof(object));
                ilGenerator.Emit(OpCodes.Stloc, paramlists);
                for (int i = 0; i < paramTypes.Length; ++i)
                {
                    ilGenerator.Emit(OpCodes.Ldloc, paramlists);
                    ilGenerator.Emit(OpCodes.Ldc_I4, i);
                    ilGenerator.Emit(OpCodes.Ldarg, (i + 1));
                    if (paramTypes[i].IsValueType)
                    {
                        ilGenerator.Emit(OpCodes.Box, paramTypes[i]);
                    }
                    ilGenerator.Emit(OpCodes.Stelem_Ref);
                }
                // ディスパッチメソッド呼び出しコードの生成
                ilGenerator.Emit(OpCodes.Ldarg, 0);
                ilGenerator.Emit(OpCodes.Ldfld, messageDispatcher);
                ilGenerator.Emit(OpCodes.Ldarg, 0);
                ilGenerator.Emit(OpCodes.Ldloc, methodinfo);
                ilGenerator.Emit(OpCodes.Ldloc, paramlists);
                ilGenerator.EmitCall(OpCodes.Call, dispatchMethod, null);
                // リターンコードの生成
                if ((method.ReturnType == typeof(void)) || (method.ReturnType == null))
                {
                    ilGenerator.Emit(OpCodes.Pop);
                }
                else
                {
                    if (method.ReturnType.IsValueType)
                    {
                        ilGenerator.Emit(OpCodes.Unbox_Any, method.ReturnType);
                    }
                    else
                    {
                        ilGenerator.Emit(OpCodes.Castclass, method.ReturnType);
                    }
                }
                ilGenerator.Emit(OpCodes.Ret);
                // オーバーライド
                typeBuilder.DefineMethodOverride(methodBuilder, method);

                // 基底クラスのメソッド呼び出し用のメソッドの生成
                MethodBuilder methodBase = typeBuilder.DefineMethod("__RTCOS_Base_" + method.Name, MethodAttributes.FamORAssem, method.ReturnType, paramTypes);
                ILGenerator ilGeneratorBase = methodBase.GetILGenerator();
                ilGeneratorBase.Emit(OpCodes.Ldarg, 0);
                for (int i = 0; i < paramTypes.Length; ++i)
                {
                    ilGeneratorBase.Emit(OpCodes.Ldarg, (i + 1));
                }
                ilGeneratorBase.EmitCall(OpCodes.Call, method, null);
                ilGeneratorBase.Emit(OpCodes.Ret);
            }

            // 型生成
            Type type = typeBuilder.CreateType();
            _LayeredObjectTypes[baseObjectType] = type;
            return type;
        }

        public override object CreateObject(Type baseType, params object[] args)
        {
            var baseLayerName = baseType.FullName.Split(new string[] { "+" }, StringSplitOptions.RemoveEmptyEntries)[0];
            var baseLayer = _LayerList.BaseLayers.Find((layer) => layer.Name == baseLayerName);
            // インスタンス化
            Func<Type, Layer, object> createInstance = (type, layer) =>
            {
                Aspect aspect = layer.ObserverAspect;
                if (aspect != null)
                {
                    return Weaver.CreateInstance(type, args, aspect);
                }
                else
                {
                    return Weaver.CreateInstance(type, args);
                }
            };

            // レイヤに属していない場合はそのまま返す
            if (baseLayer == null)
            {
                return Activator.CreateInstance(baseType);
            }

            var length = _LayerList.PartialLayers.Count + 1;
            var instances = new List<object>(length);
            // ベースオブジェクトインスタンス化
            //var baseInstance = Activator.CreateInstance(_LayeredObjectTypes[baseType]);
            var baseInstance = createInstance(_LayeredObjectTypes[baseType], baseLayer);
            instances.Add(baseInstance);
            // パーシャルオブジェクトのインスタンス化
            foreach (var partialLayer in _LayerList.PartialLayers)
            {
                object instance = null;
                var type = partialLayer.GetType().GetNestedType(baseType.Name);
                if (type != null)
                {
                    //instance = Activator.CreateInstance(type);
                    instance = createInstance(type, partialLayer);
                }
                instances.Add(instance);
                // イベントマネージャへの登録
                var activationListener = instance as ILayerActivationListener;
                if (activationListener != null)
                    _LayerEventManager.ResisterActivationListener(partialLayer, activationListener);
                var deactivationListener = instance as ILayerDeactivationListener;
                if (deactivationListener != null)
                    _LayerEventManager.ResisterDeactivationListener(partialLayer, deactivationListener);
            }
            // 参照設定
            var f1 = _LayeredObjectTypes[baseType].GetField("__RTCOS_MessageDispatcher_Field", BindingFlags.Instance | BindingFlags.NonPublic);
            f1.SetValue(baseInstance, _MessageDispatcher);
            var f2 = _LayeredObjectTypes[baseType].GetField("__RTCOS_Instances_Field", BindingFlags.Instance | BindingFlags.NonPublic);
            f2.SetValue(baseInstance, instances);

            return baseInstance;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public override object CreateObject(Type baseType, params object[] args)
        {
            var baseLayerName = baseType.FullName.Split(new string[] { "+" }, StringSplitOptions.RemoveEmptyEntries)[0];
            var baseLayer = _LayerList.BaseLayers.Find((layer) => layer.Name == baseLayerName);
            return new LayeredObject(_MessageDispatcher, baseLayer, _LayerList.PartialLayers.ToArray(), baseType, args);
        }*/

        #endregion
    }
}
