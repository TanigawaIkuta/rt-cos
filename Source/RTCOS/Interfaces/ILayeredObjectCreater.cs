﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤ状オブジェクトクリエータのインターフェース。
    /// 指定したクラスをレイヤ状オブジェクトとしてインスタンス化する。
    /// </summary>
    public abstract class ILayeredObjectCreater
    {
        /// <summary>
        /// レイヤ状オブジェクトを生成する。
        /// </summary>
        /// <param name="baseType">生成するクラスの型情報。</param>
        /// <param name="args">コンストラクタに渡す引数。</param>
        /// <returns>生成したレイヤ状オブジェクト。</returns>
        public abstract object CreateObject(Type baseType, params object[] args);
    }


    /*
    /// <summary>
    /// レイヤ状オブジェクトクリエータのインターフェース。
    /// 指定したクラスをレイヤ状オブジェクトとしてインスタンス化する。
    /// </summary>
    public interface ILayeredObjectCreater
    {
        /// <summary>
        /// レイヤ状オブジェクトを生成する。
        /// </summary>
        /// <param name="baseType">生成するクラスの型情報。</param>
        /// <param name="args">コンストラクタに渡す引数。</param>
        /// <returns>生成したレイヤ状オブジェクト。</returns>
        object CreateObject(Type baseType, params object[] args);
    }*/
}
