﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Loom;

namespace RTCOS
{
    /// <summary>
    /// オブザーバアスペクト
    /// </summary>
    public abstract class ObserverAspect : Aspect
    {
        #region コンストラクタ
        public ObserverAspect()
        {
        }

        #endregion
    }
}
