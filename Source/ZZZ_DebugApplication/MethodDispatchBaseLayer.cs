﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using RTCOS;

namespace DebugApplication
{
    public class MethodDispatchBaseLayer : BaseLayer, ILayerEntryPoint, ILayersInitializer
    {
        #region レイヤの登録
        List<BaseLayer> _BaseLayers = new List<BaseLayer>();
        List<PartialLayer> _PartialLayers = new List<PartialLayer>();

        /// <summary>
        /// ベースレイヤ
        /// </summary>
        BaseLayer[] ILayersInitializer.BaseLayers { get { return _BaseLayers.ToArray(); } }

        /// <summary>
        /// パーシャルレイヤ
        /// </summary>
        PartialLayer[] ILayersInitializer.PartialLayers { get { return _PartialLayers.ToArray(); } }

        /// <summary>
        /// レイヤの生成
        /// </summary>
        void ILayersInitializer.CreateLayers()
        {
            _BaseLayers.Add(this);
            // 下にあるものが優先的に実行される
            _PartialLayers.Add(new MethodDispatchLayer1());
            _PartialLayers.Add(new MethodDispatchLayer2());
            _PartialLayers.Add(new MethodDispatchLayer3());
            _PartialLayers.Add(new MethodDispatchLayer4());
            _PartialLayers.Add(new MethodDispatchLayer5());
            _PartialLayers.Add(new MethodDispatchLayer6());
            _PartialLayers.Add(new MethodDispatchLayer7());
            _PartialLayers.Add(new MethodDispatchLayer8());
            _PartialLayers.Add(new MethodDispatchLayer9());
        }

        #endregion

        #region ユーザプログラムのエントリポイント
        // レイヤ登録処理完了後に初めに実行される (ユーザプログラムのエントリポイント)
        void ILayerEntryPoint.LayerMain(string[] args)
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            Stopwatch stopwatch = new Stopwatch();                                                        // ベンチマーク用ストップウォッチ
            stopwatch.Stop();
            stopwatch.Reset();

            Benchmark benchmark1 = new Benchmark();                                                         // 通常のC#のベンチマーク
            Benchmark benchmark2 = (Benchmark)Layer.LayeredObjectCreater.CreateObject(typeof(Benchmark));   // フレームワークのベンチマーク
            long time1 = 0; long time2 = 0;
            // レイヤアクティベート関数
            var activateLayer = new Action<int>((layernum) =>
            {
                if (layernum > 0)
                {
                    Layer.LayerActivater.ActivateLayer("DebugApplication.MethodDispatchLayer" + layernum);
                    Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.MethodDispatchBaseLayer");     // アクティブ待ち
                }
            });

            // ベンチマーク開始
            // 極力ベンチマーク結果に影響を与えないためにベタ書き…
            Console.WriteLine("レイヤ数,使用時の実行時間,未使用時の実行時間," + Stopwatch.Frequency);
            // キャッシュミス
            Console.WriteLine("キャッシュミス");
            activateLayer(1);
            activateLayer(2);
            activateLayer(3);
            activateLayer(4);
            activateLayer(5);
            activateLayer(6);
            activateLayer(7);
            activateLayer(8);
            activateLayer(9);
            stopwatch.Start();
            benchmark1.Method9();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("9," + time2 + "," + time1);
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer1");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer2");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer3");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer4");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer5");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer6");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer7");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer8");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.MethodDispatchLayer9");
            Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.MethodDispatchBaseLayer");
            // キャッシュミス対策後
            Console.WriteLine("キャッシュミス対策後");
            // レイヤ数0
            stopwatch.Start();
            benchmark1.Method0();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("0," + time2 + "," + time1);
            // レイヤ数1
            activateLayer(1);
            stopwatch.Start();
            benchmark1.Method1();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("1," + time2 + "," + time1);
            // レイヤ数2
            activateLayer(2);
            stopwatch.Start();
            benchmark1.Method2();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("2," + time2 + "," + time1);
            // レイヤ数3
            activateLayer(3);
            stopwatch.Start();
            benchmark1.Method3();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("3," + time2 + "," + time1);
            // レイヤ数4
            activateLayer(4);
            stopwatch.Start();
            benchmark1.Method4();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("4," + time2 + "," + time1);
            // レイヤ数5
            activateLayer(5);
            stopwatch.Start();
            benchmark1.Method5();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("5," + time2 + "," + time1);
            // レイヤ数6
            activateLayer(6);
            stopwatch.Start();
            benchmark1.Method6();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("6," + time2 + "," + time1);
            // レイヤ数7
            activateLayer(7);
            stopwatch.Start();
            benchmark1.Method7();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("7," + time2 + "," + time1);
            // レイヤ数8
            activateLayer(8);
            stopwatch.Start();
            benchmark1.Method8();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("8," + time2 + "," + time1);
            // レイヤ数9
            activateLayer(9);
            stopwatch.Start();
            benchmark1.Method9();
            stopwatch.Stop();
            time1 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            stopwatch.Start();
            benchmark2.LayeredMethod();
            stopwatch.Stop();
            time2 = stopwatch.ElapsedTicks;
            stopwatch.Reset();
            Console.WriteLine("9," + time2 + "," + time1);

            // 動作確認用
            //Console.WriteLine("");
            //Console.WriteLine(benchmark1.ToString());
            //Console.WriteLine(benchmark2.ToString());
        }

        #endregion

        #region ベンチマーク用クラス
        public class Benchmark
        {
            public int _Counter0;
            public int _Counter1;
            public int _Counter2;
            public int _Counter3;
            public int _Counter4;
            public int _Counter5;
            public int _Counter6;
            public int _Counter7;
            public int _Counter8;
            public int _Counter9;

            public virtual void LayeredMethod()
            {
                ++_Counter0;
            }

            public void Method0()
            {
                ++_Counter0;
            }
            public void Method1()
            {
                ++_Counter1;
                Method0();
            }
            public void Method2()
            {
                ++_Counter2;
                Method1();
            }
            public void Method3()
            {
                ++_Counter3;
                Method2();
            }
            public void Method4()
            {
                ++_Counter4;
                Method3();
            }
            public void Method5()
            {
                ++_Counter5;
                Method4();
            }
            public void Method6()
            {
                ++_Counter6;
                Method5();
            }
            public void Method7()
            {
                ++_Counter7;
                Method6();
            }
            public void Method8()
            {
                ++_Counter8;
                Method7();
            }
            public void Method9()
            {
                ++_Counter9;
                Method8();
            }

            public override string ToString()
            {
                return _Counter0 + ", " + _Counter1 + ", " + _Counter2 + ", " + _Counter3 + ", " + _Counter4 + ", " + _Counter5 + ", " + _Counter6 + ", " + _Counter7 + ", " + _Counter8 + ", " + _Counter9;
            }
        }
        #endregion
    }
}
