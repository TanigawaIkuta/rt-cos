﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTCOS.Framework
{
    internal class LayerEventManager
    {
        #region フィールド
        private Dictionary<Layer, List<ILayerActivationListener>> _LayerActivationListeners = new Dictionary<Layer, List<ILayerActivationListener>>();
        private Dictionary<Layer, List<ILayerDeactivationListener>> _LayerDeactivationListeners = new Dictionary<Layer, List<ILayerDeactivationListener>>();

        #endregion

        #region コンストラクタ
        public LayerEventManager()
        {
        }

        #endregion

        #region メソッド
        public void ResisterActivationListener(Layer layer, ILayerActivationListener listener)
        {
            List<ILayerActivationListener> listeners = null;
            if(!_LayerActivationListeners.TryGetValue(layer, out listeners))
            {
                listeners = new List<ILayerActivationListener>();
                _LayerActivationListeners[layer] = listeners;
            }
            listeners.Add(listener);
        }

        public void ResisterDeactivationListener(Layer layer, ILayerDeactivationListener listener)
        {
            List<ILayerDeactivationListener> listeners = null;
            if (!_LayerDeactivationListeners.TryGetValue(layer, out listeners))
            {
                listeners = new List<ILayerDeactivationListener>();
                _LayerDeactivationListeners[layer] = listeners;
            }
            listeners.Add(listener);
        }

        public void OnLayerActivation(Layer layer)
        {
            var listeners = _LayerActivationListeners[layer];
            foreach(var listener in listeners)
            {
                listener.OnLayerActivation(layer);
            }
        }

        public void OnLayerDeactivation(Layer layer)
        {
            var listeners = _LayerDeactivationListeners[layer];
            foreach (var listener in listeners)
            {
                listener.OnLayerDeactivation(layer);
            }
        }

        #endregion
    }
}
