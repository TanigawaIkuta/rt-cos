﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;

namespace DebugApplication
{
    public class MethodDispatchLayer1 : PartialLayer
    {
        public MethodDispatchLayer1()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer2 : PartialLayer
    {
        public MethodDispatchLayer2()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer3 : PartialLayer
    {
        public MethodDispatchLayer3()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer4 : PartialLayer
    {
        public MethodDispatchLayer4()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer5 : PartialLayer
    {
        public MethodDispatchLayer5()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter5;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer6 : PartialLayer
    {
        public MethodDispatchLayer6()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter6;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer7 : PartialLayer
    {
        public MethodDispatchLayer7()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter7;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer8 : PartialLayer
    {
        public MethodDispatchLayer8()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter8;
                Layer.LayerdObjectAccessor.Proceed();
            }
        }
    }

    public class MethodDispatchLayer9 : PartialLayer
    {
        public MethodDispatchLayer9()
            : base("DebugApplication.MethodDispatchBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void LayeredMethod()
            {
                ++((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter9;
                Layer.LayerdObjectAccessor.Proceed();
            }

            public override string ToString()
            {
                return ((MethodDispatchBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis).ToString();
            }
        }
    }
}
