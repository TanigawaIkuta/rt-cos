﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS.Framework.Scheduling
{
    /// <summary>
    /// 
    /// </summary>
    internal class LayerActivater : ILayerActivater
    {
        #region フィールド
        /// <summary>
        /// コンテキストリスナ
        /// </summary>
        private ContextListener _ContextListener;

        /// <summary>
        /// レイヤリスト
        /// </summary>
        private LayerList _LayerList;

        #endregion

        #region コンストラクタ
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contextListener"></param>
        internal LayerActivater(ContextListener contextListener, LayerList layerList)
        {
            _ContextListener = contextListener;
            _LayerList = layerList;
        }

        #endregion

        #region ILayerActivaterの実装
        /// <summary>
        /// レイヤアクティベート
        /// </summary>
        /// <param name="layerName">アクティベートするレイヤ</param>
        public override void ActivateLayer(string layerName)
        {
            _ContextListener.LayerOperationQueue.Enqueue(new LayerOperation(LayerOperationKind.Activate, layerName));
            if (_ContextListener._CompletionFlag)
            {
                _ContextListener._CompletionFlag = false;
                LayerScheduler._Thread.Interrupt();
            }
        }

        /// <summary>
        /// レイヤディアクティベート
        /// </summary>
        /// <param name="layerName">ディアクティベートするレイヤ</param>
        public override void DeactivateLayer(string layerName)
        {
            _ContextListener.LayerOperationQueue.Enqueue(new LayerOperation(LayerOperationKind.Deactivate, layerName));
            if (_ContextListener._CompletionFlag)
            {
                _ContextListener._CompletionFlag = false;
                LayerScheduler._Thread.Interrupt();
            }
        }

        /// <summary>
        /// レイヤクリティカルセクションに入ることを通知する
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        public override void EnterLayerCriticalSection(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.EnterLayerCriticalSection(id);
        }

        /// <summary>
        /// レイヤクリティカルセクションから出ることを通知する
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        public override void ExitLayerCriticalSection(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.ExitLayerCriticalSection(id);
        }

        /// <summary>
        /// レイヤ操作の完了待ち
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        public override void WaitLayerOperationCompletion(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.WaitLayerOperationCompletion(id);
        }

        /*
        /// <summary>
        /// レイヤアクティベート
        /// </summary>
        /// <param name="layerName">アクティベートするレイヤ</param>
        void ILayerActivater.ActivateLayer(string layerName)
        {
            _ContextListener.LayerOperationQueue.Enqueue(new LayerOperation(LayerOperationKind.Activate, layerName));
            if (_ContextListener._CompletionFlag)
            {
                _ContextListener._CompletionFlag = false;
                LayerScheduler._Thread.Interrupt();
            }
        }

        /// <summary>
        /// レイヤディアクティベート
        /// </summary>
        /// <param name="layerName">ディアクティベートするレイヤ</param>
        void ILayerActivater.DeactivateLayer(string layerName)
        {
            _ContextListener.LayerOperationQueue.Enqueue(new LayerOperation(LayerOperationKind.Deactivate, layerName));
            if (_ContextListener._CompletionFlag)
            {
                _ContextListener._CompletionFlag = false;
                LayerScheduler._Thread.Interrupt();
            }
        }

        /// <summary>
        /// レイヤクリティカルセクションに入ることを通知する
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        void ILayerActivater.EnterLayerCriticalSection(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.EnterLayerCriticalSection(id);
        }

        /// <summary>
        /// レイヤクリティカルセクションから出ることを通知する
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        void ILayerActivater.ExitLayerCriticalSection(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.ExitLayerCriticalSection(id);
        }

        /// <summary>
        /// レイヤ操作の完了待ち
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        void ILayerActivater.WaitLayerOperationCompletion(string baseLayerName)
        {
            var id = _LayerList.BaseLayers.FindIndex((layer) => layer.Name == baseLayerName);
            _ContextListener.WaitLayerOperationCompletion(id);
        }*/

        #endregion
    }
}
