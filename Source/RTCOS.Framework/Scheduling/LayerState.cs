﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS.Framework.Scheduling
{
    /// <summary>
    /// レイヤの状態
    /// </summary>
    public class LayerState
    {
        #region 状態の種類
        /// <summary>
        /// 状態無し
        /// </summary>
        public static readonly LayerState None = new LayerState(0, "None");

        /// <summary>
        /// アクティブ
        /// </summary>
        public static readonly LayerState Active = new LayerState(0x0001, "Active");

        /// <summary>
        /// 非アクティブ
        /// </summary>
        public static readonly LayerState Inactive = new LayerState(0x0002, "Inactive");

        /// <summary>
        /// アクティベート待ち
        /// </summary>
        public static readonly LayerState WaitActivation = new LayerState(0x0101, "WaitActivation");

        /// <summary>
        /// ディアクティベート待ち
        /// </summary>
        public static readonly LayerState WaitDeactivation = new LayerState(0x0102, "WaitDeactivation");

        #endregion

        #region プロパティ
        /// <summary>
        /// 状態を識別するためのID
        /// </summary>
        public ushort ID { get; private set; }

        /// <summary>
        /// 状態を表す名前
        /// </summary>
        public string Name { get; private set; }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// レイヤの状態
        /// </summary>
        /// <param name="id">状態を識別するためのID</param>
        /// <param name="name">状態を表す名前</param>
        protected LayerState(ushort id, string name)
        {
            ID = id;
            Name = name;
        }

        #endregion

        #region 型変換
        #endregion

        #region オーバーライド
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            // objがnullか、型が違うときは、等価でない
            if ((obj == null) || (this.GetType() != obj.GetType()))
            {
                return false;
            }

            // IDで比較する
            LayerState state = (LayerState)obj;
            return (this.ID == state.ID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
