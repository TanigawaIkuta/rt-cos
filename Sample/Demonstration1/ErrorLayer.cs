﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RTCOS;

namespace Demonstration1
{
    public class ErrorLayer : PartialLayer
    {
        #region コンストラクタ
        // コンストラクタ
        public ErrorLayer()
            : base("Demonstration1.AppBaseLayer", 2)
        {
        }

        #endregion

        #region クラス群
        // アプリケーションクラス
        public class App
        {
            // 実行用メソッド
            public virtual void Run()
            {
                // 停止処理
                dynamic robot = ((AppBaseLayer.App)Layer.LayerdObjectAccessor.LayerdThis).robot;
                if (robot.IsStartSensorUpdate())
                {
                    robot.StopSensorUpdate();
                }
                Console.WriteLine("*****************");
                Console.WriteLine("***** Error *****");
                Console.WriteLine("*****************");
                Thread.Sleep(Timeout.Infinite);
            }
        }

        #endregion
    }
}
