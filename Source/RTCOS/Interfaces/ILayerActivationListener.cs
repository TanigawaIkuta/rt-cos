﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTCOS
{
    public interface ILayerActivationListener
    {
        void OnLayerActivation(Layer layer);
    }

    public interface ILayerDeactivationListener
    {
        void OnLayerDeactivation(Layer layer);
    }
}
