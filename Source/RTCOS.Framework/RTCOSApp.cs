﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS.Framework.Scheduling;

namespace RTCOS.Framework
{
    /// <summary>
    /// ContextCSアプリケーション。
    /// </summary>
    public class RTCOSApp
    {
        #region フィールド
        /// <summary>
        /// ContextCSのマネージャ部分。
        /// </summary>
        private RTCOSManager _ContextCSManager;

        #endregion

        #region コンストラクタ
        /// <summary>
        /// ContextCS.ContextCSApp のコンストラクタ。
        /// </summary>
        /// <param name="entryPoint">ContextCSアプリケーションのエントリポイント。</param>
        public RTCOSApp(ILayersInitializer layersInitializer)
            : this(layersInitializer, new DefaultLayerSchedulingAlgorithm())
        {
        }

        /// <summary>
        /// ContextCS.ContextCSApp のコンストラクタ。
        /// </summary>
        /// <param name="layersInitializer">レイヤ初期化インターフェース。</param>
        /// <param name="layerSchedulingAlgorithm">レイヤスケジューリングのためのアルゴリズム。</param>
        public RTCOSApp(ILayersInitializer layersInitializer, LayerSchedulingAlgorithm layerSchedulingAlgorithm)
        {
            // マネージャ部分のインスタンス化
            _ContextCSManager = new RTCOSManager(layersInitializer, layerSchedulingAlgorithm);
        }

        #endregion

        #region メソッド
        /// <summary>
        /// ContextCSアプリケーションの実行。
        /// </summary>
        /// <param name="args">コマンドライン引数。</param>
        public void Execute(string[] args)
        {
            try
            {
                // マネージャ部分の初期化
                _ContextCSManager.Initialize();
                // 実行
                _ContextCSManager.Execute(args);
            }
            finally
            {
                // 終了処理
            }
        }

        #endregion
    }
}
