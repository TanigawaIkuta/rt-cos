﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;
using RTCOS.Framework;

namespace Demonstration1
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new RTCOSApp(new LayersInitializer());
            app.Execute(args);
        }
    }
}
