﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;

namespace DebugApplication
{
    public class UserPartialLayer2 : PartialLayer
    {
        public UserPartialLayer2()
            : base("DebugApplication.UserBaseLayer", 1)
        {
        }

        public class A
        {
            public void M1()
            {
                Console.WriteLine("UserPartialLayer2.A.M1");
                UserBaseLayer.A obj = (UserBaseLayer.A)Layer.LayerdObjectAccessor.LayerdThis;
                obj.M2();
            }

            public void M2()
            {
                Console.WriteLine("UserPartialLayer2.A.M2");
            }

            public void M3()
            {
                Console.WriteLine("UserPartialLayer2.A.M3");
                Console.WriteLine("Proceed");
                Layer.LayerdObjectAccessor.Proceed();
                Console.WriteLine("Proceed");
            }
        }
    }
}
