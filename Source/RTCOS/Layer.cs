﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤ
    /// </summary>
    public abstract class Layer
    {
        #region プロパティ
        /// <summary>
        /// レイヤ名
        /// </summary>
        public virtual string Name { get { return this.GetType().FullName; } }

        /// <summary>
        /// オブザーバアスペクト
        /// </summary>
        public ObserverAspect ObserverAspect { get; protected set; }

        /// <summary>
        /// レイヤリスト
        /// </summary>
        public static ILayerList LayerList { get; private set; }

        /// <summary>
        /// レイヤアクティベータ
        /// </summary>
        public static ILayerActivater LayerActivater { get; private set; }

        /// <summary>
        /// レイヤ上オブジェクトの生成
        /// </summary>
        public static ILayeredObjectCreater LayeredObjectCreater { get; private set; }

        /// <summary>
        /// レイヤ上オブジェクトのアクセサ
        /// </summary>
        public static ILayerdObjectAccessor LayerdObjectAccessor { get; private set; }

        #endregion

        #region コンストラクタ
        /// <summary>
        /// レイヤ
        /// </summary>
        public Layer()
        {
        }

        #endregion
    }
}
