﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤエントリポイントのインターフェース。
    /// ベースレイヤでこのインターフェースを実装することで、RTCOSアプリケーション実行時の処理を記述する。
    /// </summary>
    public interface ILayerEntryPoint
    {
        #region メソッド
        /// <summary>
        /// レイヤエントリポイント。
        /// ベースレイヤが持つこのメソッドからプログラムを開始する。
        /// </summary>
        /// <param name="args">コマンドライン引数。</param>
        void LayerMain(string[] args);

        #endregion
    }
}
