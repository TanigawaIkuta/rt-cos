﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Loom;
using Loom.JoinPoints;
using RTCOS;

namespace Demonstration1
{
    // 吸引モードの掃除を行うレイヤ
    public class VacuumLayer : PartialLayer
    {
        #region コンストラクタ
        // コンストラクタ
        public VacuumLayer()
            : base("Demonstration1.AppBaseLayer", 1)        // ベースレイヤ名, 優先度
        {
            // オブザーバアスペクトの指定
            ObserverAspect = new VacuumObserverAspect();
        }

        #endregion

        #region オブザーバアスペクト
        // オブザーバアスペクト
        public class VacuumObserverAspect : ObserverAspect
        {
            // 状態
            private enum State
            {
                Vacuum,
                NotVacuum1,
                NotVacuum2,
                NotVacuum3,
                WipeMode,
                Error,
            }
            private State _State = State.Vacuum;

            // クリティカルセクションに入る処理
            [Include(typeof(App), "Run")]
            [Include(typeof(CleanningRobot), "SensorThreadMain")]
            [Call(Advice.Around)]
            public T CriticalSection<T>([JPContext]Context ctx, params object[] args) where T : _Void
            {
                Layer.LayerActivater.EnterLayerCriticalSection("Demonstration1.AppBaseLayer");
                // ターゲットメソッドの実行
                ctx.Invoke(args);
                Layer.LayerActivater.ExitLayerCriticalSection("Demonstration1.AppBaseLayer");
                return (T)_Void.Value;
            }

            // センサ情報を確認し、レイヤの切り替えを判断する処理
            [Include(typeof(Sensor), "GetSensorData")]
            [Call(Advice.Around)]
            public T CheckSensor<T>([JPContext]Context ctx, params object[] args)
            {
                // ターゲットメソッドを実行し、センサ値を取得
                sbyte value = (sbyte)ctx.Invoke(args);
                // センサ値による状態の変化
                switch (_State)
                {
                    case State.Vacuum:          // ゴミ有り
                        if (value == 0)
                        {
                            _State = State.NotVacuum1;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.NotVacuum1:      // ゴミ無し
                        if (value == 0)
                        {
                            _State = State.NotVacuum2;
                        }
                        else if (value == 1)
                        {
                            _State = State.Vacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.NotVacuum2:      // ゴミ無し
                        if (value == 0)
                        {
                            _State = State.NotVacuum3;
                        }
                        else if (value == 1)
                        {
                            _State = State.Vacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.NotVacuum3:      // ゴミ無し
                        if (value == 0)
                        {
                            // ゴミなしが続いたので、拭き掃除モードに移る
                            _State = State.WipeMode;
                            // 拭き掃除レイヤのアクティベート
                            Layer.LayerActivater.DeactivateLayer("Demonstration1.VacuumLayer");
                            Layer.LayerActivater.ActivateLayer("Demonstration1.WipeLayer");
                        }
                        else if (value == 1)
                        {
                            _State = State.Vacuum;
                        }
                        else if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.WipeMode:
                        if (value == -1)
                        {
                            _State = State.Error;
                            Layer.LayerActivater.ActivateLayer("Demonstration1.ErrorLayer");
                        }
                        break;
                    case State.Error:
                        break;
                    default:
                        break;
                }
                // センサ値を返す
                return (T)((object)value);
            }
        }

        #endregion

        #region クラス群
        // アプリケーションクラス
        public class App
        {
            // 実行用メソッド (オブザーバアスペクトでウィーブするために、ベースメソッドをそのまま呼び出すメソッドを定義している)
            public virtual void Run()
            {
                // ベースメソッドをそのまま実行
                Layer.LayerdObjectAccessor.Proceed();
                //((AppBaseLayer.App)Layer.LayerdObjectAccessor.LayerdBase).Run();
            }
        }

        // 掃除ロボット
        public class CleanningRobot
        {
            // 掃除
            public virtual void Clean()
            {
                // 吸引モードによる掃除
                Console.WriteLine("**** Vacuum ****");
                var sensorValue = ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._SensorValue;
                Console.WriteLine("SensorValue = " + sensorValue);
                Console.WriteLine("****************");
            }

            // センサスレッドの処理
            public virtual void SensorThreadMain()
            {
                // ベースクラスのセンサクラスの参照から、センサ値を取得
                var sensorValue = ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._Sensor.GetSensorData();
                // ベースクラスのフィールドにセンサ値を格納
                ((AppBaseLayer.CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis)._SensorValue = sensorValue;
                Thread.Sleep(500);
            }
        }

        // 吸引可能なゴミを探知するためのセンサクラス
        public class Sensor
        {
            // センサデータの取得
            public virtual sbyte GetSensorData()
            {
                return (sbyte)Layer.LayerdObjectAccessor.Proceed();
                // ベースメソッドをそのまま実行
                //return ((AppBaseLayer.Sensor)Layer.LayerdObjectAccessor.LayerdBase).GetSensorData();
            }
        }

        #endregion

    }
}
