﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Linq.Expressions;

namespace RTCOS.Framework.Dispatch
{
    /// <summary>
    /// メッセージディスパッチャ。
    /// </summary>
    public class MessageDispatcher : ILayerdObjectAccessor
    {
        #region フィールド
        /// <summary>
        /// レイヤリスト
        /// </summary>
        private LayerList _LayerList;

        /// <summary>
        /// メソッドリスト。
        /// </summary>
        private Dictionary<MethodInfo, List<Func<object, object[], object>>> _MethodLists = new Dictionary<MethodInfo, List<Func<object, object[], object>>>();

        //private Dictionary<PropertyInfo, List<PropertyInfo>> _PartialProperties = new Dictionary<PropertyInfo, List<PropertyInfo>>();

        private Dictionary<Thread, Stack<CallingInfo>> _CallingInfoStacks = new Dictionary<Thread, Stack<CallingInfo>>();

        internal Dictionary<MethodInfo, MethodInfo> _BaseMethods = new Dictionary<MethodInfo,MethodInfo>();

        private int _LayerNum;

        /// <summary>
        /// 同期オブジェクト
        /// </summary>
        private object _SyncObject = new object();

        #endregion

        #region コンストラクタ
        /// <summary>
        /// 
        /// </summary>
        public MessageDispatcher(LayerList layerList)
        {
            _LayerList = layerList;
        }

        #endregion

        #region メソッド
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="methodInfo"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public object DispatchMethod(object instance, MethodInfo methodInfo, object[] args)
        {
            // 現在のスレッドの呼び出し情報スタックが無ければ作る (ここで時間食ってる)
            Stack<CallingInfo> callingInfoStack = null;
            var currentThread = Thread.CurrentThread;
            if (!_CallingInfoStacks.TryGetValue(currentThread, out callingInfoStack))
            {
                callingInfoStack = new Stack<CallingInfo>();
                _CallingInfoStacks[currentThread] = callingInfoStack;
            }
            // 呼び出し情報をスタックにプッシュ
            var methodList = _MethodLists[methodInfo];
            var index = _LayerNum - 1;
            var info = new CallingInfo(instance, _BaseMethods[methodInfo], methodList, index);
            callingInfoStack.Push(info);
            // メソッド実行
            var retVal = CallMethod(info, args);
            // 呼び出し情報をスタックからポップ
            callingInfoStack.Pop();

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private object CallMethod(CallingInfo info, object[] args)
        {
            object retVal = null;

            for (int i = info.Index; i >= 0; --i)
            {
                // アクティブでないレイヤは無視
                if (!_LayerList.CheckActiveFlag(i))
                {
                    continue;
                }

                // 有効なレイヤに属するメソッドを取得
                var method = info.MethodList[i];
                // メソッド実行
                if (method != null)
                {
                    var instances = ((ILayerdObject)info.Instance).__RTCOS_Instances;
                    var instance = instances[i];
                    int index = info.Index;
                    info.Index = i - 1;
                    retVal = method(instance, args);
                    info.Index = index;
                    break;
                }
            }

            return retVal;
        }

        public void Initialize()
        {
            var baseLayerTypes = from baseLayer in _LayerList.BaseLayers
                                 select baseLayer.GetType();
            var partialLayerTypes = from partialLayer in _LayerList.PartialLayers
                                    select partialLayer.GetType();
            RegisterPartialMethods(baseLayerTypes, partialLayerTypes);
        }

        private void RegisterPartialMethods(IEnumerable<Type> baseLayerTypes, IEnumerable<Type> partialLayerTypes)
        {
            var baseObjectTypes = from baseLayerType in baseLayerTypes
                                  from type in baseLayerType.GetNestedTypes()
                                  where !type.IsSubclassOf(typeof(ObserverAspect))
                                  select type;

            _LayerNum = partialLayerTypes.Count() + 1;
            foreach(var baseObjectType in baseObjectTypes)
            {
                var baseMethods = from method in baseObjectType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                  where method.Attributes.HasFlag(MethodAttributes.Virtual)
                                  where method.Name != "ToString"
                                  where method.Name != "Equals"
                                  where method.Name != "GetHashCode"
                                  where method.Name != "GetType"
                                  where method.Name != "Finalize"
                                  select method;
                foreach (var baseMethod in baseMethods)
                {
                    _MethodLists[baseMethod] = new List<Func<object, object[], object>>(_LayerNum);
                    _MethodLists[baseMethod].Add(CreateDelegate(_BaseMethods[baseMethod].DeclaringType, _BaseMethods[baseMethod]));
                    var types = from parameter in baseMethod.GetParameters()
                                select parameter.ParameterType;
                    foreach (var partialLayerType in partialLayerTypes)
                    {
                        var partialObjectType = partialLayerType.GetNestedType(baseObjectType.Name);
                        if (partialObjectType != null)
                        {
                            // パーシャルメソッドの情報を取得
                            var partialMethod = partialObjectType.GetMethod(baseMethod.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, types.ToArray(), null);
                            if (partialMethod != null)
                            {
                                // デリゲートの生成
                                _MethodLists[baseMethod].Add(CreateDelegate(partialObjectType, partialMethod));
                                continue;
                            }
                        }
                        _MethodLists[baseMethod].Add(null);
                    }
                }
            }
        }

        private Func<object, object[], object> CreateDelegate(Type type, MethodInfo method)
        {
            // インスタンスとパラメータ
            var instance = Expression.Parameter(typeof(object), "instance");
            var args = Expression.Parameter(typeof(object[]), "args");
            var retVal = Expression.Variable(typeof(object), "retVal");
            // 変換用
            var cinstance = Expression.TypeAs(instance, type);
            var parameters = method.GetParameters()
                                .Select((x, index) =>
                                    Expression.Convert(
                                        Expression.ArrayIndex(args, Expression.Constant(index)),
                                    x.ParameterType))
                               .ToArray();
            // 本体の作成
            Func<object, object[], object> lambda = null;
            if (method.ReturnType != typeof(void))
            {
                lambda = Expression.Lambda<Func<object, object[], object>>(
                            Expression.TypeAs(
                                Expression.Call(cinstance, method, parameters),
                                typeof(object)),
                            instance, args).Compile();
            }
            else
            {
                lambda = Expression.Lambda<Func<object, object[], object>>(
                            Expression.Block(
                                Expression.Call(cinstance, method, parameters),
                                Expression.Constant(null)),
                            instance, args).Compile();
            }

            // 結果を返す
            return lambda;
        }

        /*
        private void RegisterProperties(IEnumerable<Type> baseLayerTypes, IEnumerable<Type> partialLayerTypes)
        {
            var baseObjectTypes = from baseLayerType in baseLayerTypes
                                  from type in baseLayerType.GetNestedTypes()
                                  where !type.IsSubclassOf(typeof(ObserverAspect))
                                  select type;

            var size = partialLayerTypes.Count();
            foreach (var baseObjectType in baseObjectTypes)
            {
                var baseProperties = from property in baseObjectType.GetProperties()
                                     select property;
                foreach (var baseProperty in baseProperties)
                {
                    _MethodLists[baseMethod] = new List<MethodInfo>(size);
                    foreach (var partialLayerType in partialLayerTypes)
                    {
                        var partialObjectType = partialLayerType.GetNestedType(baseObjectType.Name);
                        if (partialObjectType != null)
                        {
                            var types = from parameter in baseMethod.GetParameters()
                                        select parameter.ParameterType;
                            var partialMethod = partialObjectType.GetMethod(baseMethod.Name, types.ToArray());
                            _MethodLists[baseMethod].Add(partialMethod);
                        }
                        else
                        {
                            _MethodLists[baseMethod].Add(null);
                        }
                    }
                }
            }
        }*/

        #endregion

        #region ILayerdObjectAccessorの実装
        public override object Proceed(params object[] args)
        {
            var info = _CallingInfoStacks[Thread.CurrentThread].Peek();
            var retVal = CallMethod(info, args);
            return retVal;
        }

        public override object LayerdThis
        {
            get
            {
                // ここ問題あり
                var info = _CallingInfoStacks[Thread.CurrentThread].Peek();
                return info.Instance;
            }
        }

        /*
        object ILayerdObjectAccessor.Proceed(params object[] args)
        {
            var info = _CallingInfoStacks[Thread.CurrentThread].Peek();
            var retVal = CallMethod(info, args);
            return retVal;
        }

        object ILayerdObjectAccessor.LayerdThis
        {
            get
            {
                // ここ問題あり
                var info = _CallingInfoStacks[Thread.CurrentThread].Peek();
                return info.Instance;
            }
        }

        object ILayerdObjectAccessor.LayerdBase
        {
            get
            {
                var info = _CallingInfoStacks[Thread.CurrentThread].Peek();
                return info.Instance.BaseInstance;
            }
        }*/

        #endregion

        #region 型定義
        private class CallingInfo
        {
            #region プロパティ
            public object Instance { get { return _Instance; } }
            private object _Instance;

            public MethodInfo MethodInfo { get { return _MethodInfo; } }
            private MethodInfo _MethodInfo;

            public List<Func<object, object[], object>> MethodList { get { return _MethodList; } }
            private List<Func<object, object[], object>> _MethodList;

            public int Index { get { return _Index; } set { _Index = value; } }
            private int _Index;

            #endregion

            #region コンストラクタ
            public CallingInfo(object instance, MethodInfo methodInfo, List<Func<object, object[], object>> methodList, int index)
            {
                _Instance = instance;
                _MethodInfo = methodInfo;
                _MethodList = methodList;
                _Index = index;
            }

            #endregion
        }

        #endregion
    }
}
