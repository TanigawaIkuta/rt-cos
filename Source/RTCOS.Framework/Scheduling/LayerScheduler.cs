﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RTCOS.Framework.Dispatch;

namespace RTCOS.Framework.Scheduling
{
    /// <summary>
    /// レイヤスケジューラ。
    /// </summary>
    internal class LayerScheduler
    {
        #region フィールド
        /// <summary>
        /// コンテキストリスナ
        /// </summary>
        private ContextListener _ContextListener;

        private LayerList _LayerList;

        private MessageDispatcher _MessageDispatcher;

        private LayerEventManager _LayerEventManager;

        private LayerSchedulingAlgorithm _LayerSchedulingAlgorithm;
        
        private LCB[] _WaitOperationLCB = new LCB[16];
        
        private LCB _ActiveLayerLCB;
        
        private LCB _InactiveLayerLCB;
        
        // 応急処置
        internal static Thread _Thread;

        #endregion

        #region コンストラクタ
        public LayerScheduler(ContextListener contextListener, LayerList layerList, MessageDispatcher messageDispatcher, LayerEventManager layerEventManager, LayerSchedulingAlgorithm algorithm)
        {
            _ContextListener = contextListener;
            _LayerList = layerList;
            _MessageDispatcher = messageDispatcher;
            _LayerEventManager = layerEventManager;
            _LayerSchedulingAlgorithm = algorithm;
            _LayerSchedulingAlgorithm._LayerScheduler = this;
        }

        #endregion

        #region メソッド
        public void Initialize()
        {
            int lcbid = 1;
            foreach (var layer in _LayerList.PartialLayers)
            {
                LCB lcb = new LCB();
                lcb.Layer = layer;
                lcb.BaseLayer = layer.BaseLayer;
                lcb.LayerState = LayerState.Inactive;
                lcb.ID = lcbid++;
                lcb.Priority = layer.Priority;
                lcb.NextLCB = _InactiveLayerLCB;
                _InactiveLayerLCB = lcb;
            }

            _Thread = new Thread(() =>
            {
                // 今はここに直接スケジューリングプログラムを実装している
                // いずれ、LayerSchedulingAlgorithmに移す予定
                while (true)
                {
                    try
                    {
                        if (_ContextListener._CompletionFlag)
                        {
                            Thread.Sleep(Timeout.Infinite);
                        }
                        ThreadingMethod();
                        Thread.Sleep(_LayerSchedulingAlgorithm.ThreadCycle);
                    }
                    catch (ThreadInterruptedException)
                    {
                    }
                }
            });

            _Thread.IsBackground = true;
            _Thread.Start();
        }

        private void ThreadingMethod()
        {
            var count = _ContextListener.LayerOperationQueue.Count;
            if (count > 0)
            {
                for (int i = 0; i < count; ++i)
                {
                    var operation = _ContextListener.LayerOperationQueue.Dequeue();
                    LCB lcb1 = null;
                    LCB lcb2 = null;
                    switch (operation.Kind)
                    {
                        case LayerOperationKind.Activate:
                            lcb1 = _InactiveLayerLCB;
                            lcb2 = null;
                            while (lcb1 != null)
                            {
                                if (operation.LayerName == lcb1.Layer.GetType().FullName)
                                {
                                    lcb1.LayerState = LayerState.WaitActivation;
                                    if (lcb2 != null) lcb2.NextLCB = lcb1.NextLCB;
                                    else _InactiveLayerLCB = lcb1.NextLCB;
                                    lcb1.NextLCB = _WaitOperationLCB[lcb1.Priority];
                                    _WaitOperationLCB[lcb1.Priority] = lcb1;
                                    break;
                                }
                                lcb2 = lcb1;
                                lcb1 = lcb1.NextLCB;
                            }
                            break;
                        case LayerOperationKind.Deactivate:
                            lcb1 = _ActiveLayerLCB;
                            lcb2 = null;
                            while (lcb1 != null)
                            {
                                if (operation.LayerName == lcb1.Layer.GetType().FullName)
                                {
                                    lcb1.LayerState = LayerState.WaitDeactivation;
                                    if (lcb2 != null) lcb2.NextLCB = lcb1.NextLCB;
                                    else _ActiveLayerLCB = lcb1.NextLCB;
                                    lcb1.NextLCB = _WaitOperationLCB[lcb1.Priority];
                                    _WaitOperationLCB[lcb1.Priority] = lcb1;
                                    break;
                                }
                                lcb2 = lcb1;
                                lcb1 = lcb1.NextLCB;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                var length = _WaitOperationLCB.Length;
                for (int i = (length - 1); i >= 0; --i)
                {
                    if (_WaitOperationLCB[i] != null)
                    {
                        var baseLayer = _WaitOperationLCB[i].BaseLayer;
                        var id = _LayerList.BaseLayers.IndexOf(baseLayer);
                        _ContextListener.EnterLayerOperating(id);
                        try
                        {
                            LCB lcb = _WaitOperationLCB[i];
                            LCB differentBaseLayer = null;
                            while (lcb != null)
                            {
                                LCB next = lcb.NextLCB;
                                if (baseLayer == _LayerList.BaseLayers[id])
                                {
                                    if (lcb.LayerState == LayerState.WaitActivation)
                                    {
                                        _LayerEventManager.OnLayerActivation(lcb.Layer);
                                        _LayerList.SetActiveFlag(lcb.ID);
                                        lcb.NextLCB = _ActiveLayerLCB;
                                        _ActiveLayerLCB = lcb;
                                    }
                                    else if (lcb.LayerState == LayerState.WaitDeactivation)
                                    {
                                        _LayerEventManager.OnLayerDeactivation(lcb.Layer);
                                        _LayerList.RemoveActiveFlag(lcb.ID);
                                        lcb.NextLCB = _InactiveLayerLCB;
                                        _InactiveLayerLCB = lcb;
                                    }
                                }
                                else
                                {
                                    lcb.NextLCB = differentBaseLayer;
                                    differentBaseLayer = lcb;
                                }
                                lcb = next;
                            }
                            _WaitOperationLCB[i] = differentBaseLayer;
                        }
                        finally
                        {
                            _ContextListener.ExitLayerOperating(id);
                        }
                        break;
                    }
                    // 全部終了
                    if ((i <= 0) && (_ContextListener.LayerOperationQueue.Count <= 0))
                    {
                        _ContextListener._CompletionFlag = true;
                    }
                }
            }
        }

        #endregion
    }
}
