﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RTCOS;

namespace Demonstration1
{
    // ベースレイヤ
    public class AppBaseLayer : BaseLayer, ILayerEntryPoint
    {
        #region ユーザプログラムのエントリポイント
        // レイヤ登録処理完了後に初めに実行される (ユーザプログラムのエントリポイント)
        void ILayerEntryPoint.LayerMain(string[] args)
        {
            // 初期レイヤのアクティベート
            // 初めは吸引モードで動かす
            Layer.LayerActivater.ActivateLayer("Demonstration1.VacuumLayer");
            //Thread.Sleep(2000);     // アクティブ待ち

            // 処理
            App app = (App)Layer.LayeredObjectCreater.CreateObject(typeof(App));
            while (true)
            {
                app.Run();
            }
        }

        #endregion

        #region クラス群
        // アプリケーションクラス
        public class App
        {
            // ロボットの参照
            public CleanningRobot robot = (CleanningRobot)Layer.LayeredObjectCreater.CreateObject(typeof(CleanningRobot));
            
            // 実行用メソッド
            public virtual void Run()
            {
                // ロボットを動かすための準備
                if (!robot.IsStartSensorUpdate())
                {
                    // センサアップデート開始
                    robot.StartSensorUpdate();
                }
                // 掃除
                robot.Clean();
                Thread.Sleep(500);
            }
        }

        // 掃除ロボットクラス
        public class CleanningRobot
        {
            // 吸引可能なゴミを探知するためのセンサの参照
            public Sensor _Sensor = (Sensor)Layer.LayeredObjectCreater.CreateObject(typeof(Sensor));
            // センサを周期的に更新するためのスレッド
            public Thread _SensorThread;
            // センサ値
            public sbyte _SensorValue;

            // センサアップデート開始
            public virtual void StartSensorUpdate()
            {
                // 自身への参照
                CleanningRobot layerdThis = ((CleanningRobot)Layer.LayerdObjectAccessor.LayerdThis);
                // スレッドで実行するメソッド
                _SensorThread = new Thread((obj) =>
                {
                    while (true)
                    {
                        ((dynamic)obj).SensorThreadMain();
                    }
                });
                _SensorThread.IsBackground = true;
                // スレッドの実行開始
                _SensorThread.Start(layerdThis);
            }

            // センサアップデート停止
            public virtual void StopSensorUpdate()
            {
                _SensorThread.Abort();
                _SensorThread = null;
            }

            // センサアップデート中であるか
            public virtual bool IsStartSensorUpdate()
            {
                return _SensorThread != null;
            }

            // 掃除
            public virtual void Clean()
            {
                Console.WriteLine("Cleanning");
            }

            // センサスレッドの処理
            public virtual void SensorThreadMain()
            {
                Thread.Sleep(500);
            }
        }

        // 吸引可能なゴミを探知するためのセンサクラス
        public class Sensor
        {
            // ダミーデータ (ゴミが吸い込めている時に1, 吸い込めていない時に0, センサ故障時に-1となる)
            private sbyte[] _SensorData = new sbyte[] { 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1 };
            private int _Index;

            // センサデータの取得
            public virtual sbyte GetSensorData()
            {
                sbyte value = -1;
                if (_Index < _SensorData.Length)
                {
                    value = _SensorData[_Index];
                    ++_Index;
                }
                return value;
            }
        }

        #endregion
    }
}
