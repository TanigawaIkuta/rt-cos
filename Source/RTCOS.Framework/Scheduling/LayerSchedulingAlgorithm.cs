﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS.Framework.Scheduling
{
    public abstract class LayerSchedulingAlgorithm
    {
        #region フィールド
        internal LayerScheduler _LayerScheduler;

        #endregion

        #region プロパティ
        public int ThreadCycle { get; set; }

        #endregion

        #region コンストラクタ
        public LayerSchedulingAlgorithm()
        {
            ThreadCycle = 1;
        }

        #endregion

        #region 抽象メソッド
        public abstract void Initialize(List<BaseLayer> BaseLayers, List<PartialLayer> PartialLayers);

        #endregion

        #region メソッド
        #endregion
    }
}
