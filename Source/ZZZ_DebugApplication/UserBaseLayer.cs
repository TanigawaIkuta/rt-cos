﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Loom;
using Loom.JoinPoints;
using RTCOS;

namespace DebugApplication
{
    public class UserBaseLayer : BaseLayer, ILayerEntryPoint
    {
        public UserBaseLayer()
        {
            ObserverAspect = new UserObserverAspect();
        }

        void ILayerEntryPoint.LayerMain(string[] args)
        {
            App app = (App)Layer.LayeredObjectCreater.CreateObject(typeof(App));
            A a = (A)Layer.LayeredObjectCreater.CreateObject(typeof(A));

            Layer.LayerActivater.ActivateLayer("DebugApplication.UserPartialLayer1");
            Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.UserBaseLayer");
            Layer.LayerActivater.DeactivateLayer("DebugApplication.UserPartialLayer1");
            //Layer.LayerActivater.ActivateLayer("DebugApplication.UserPartialLayer2");
            Layer.LayerActivater.WaitLayerOperationCompletion("DebugApplication.UserBaseLayer");

            for (int i = 0; i < 10; ++i)
            {
                app.Run(a);
                /* 以前の内容
                Layer.LayerActivater.EnterLayerCriticalSection("DebugApplication.UserBaseLayer");
                a.M1();
                a.M3();
                Layer.LayerActivater.ExitLayerCriticalSection("DebugApplication.UserBaseLayer");
                Console.WriteLine();
                */
                System.Threading.Thread.Sleep(1000);
            }
            Console.ReadLine();
        }

        public class UserObserverAspect : ObserverAspect
        {
            [Include(typeof(App), "Run")]
            [Call(Advice.Around)]
            public T Weave<T>([JPContext]Context ctx, params object[] args) where T : _Void
            {
                Layer.LayerActivater.EnterLayerCriticalSection("DebugApplication.UserBaseLayer");
                Console.WriteLine("*************** ObserverAspect *****************");
                ctx.Invoke(args);
                Console.WriteLine("*************** ObserverAspect *****************");
                Console.WriteLine("");
                Layer.LayerActivater.ExitLayerCriticalSection("DebugApplication.UserBaseLayer");
                return (T)_Void.Value;
            }
        }

        public class App
        {
            public virtual void Run(A a)
            {
                a.M1();
                //a.M3();
            }
        }

        public class A
        {
            public B _B = (B)Layer.LayeredObjectCreater.CreateObject(typeof(B));
            public C _C = (C)Layer.LayeredObjectCreater.CreateObject(typeof(C));

            public virtual void M1()
            {
                Console.WriteLine("UserBaseLayer.A.M1");
                dynamic obj = Layer.LayerdObjectAccessor.LayerdThis;
                obj.M2();
            }

            public virtual void M2()
            {
                Console.WriteLine("UserBaseLayer.A.M2");
                _B.M1(10);
                _C.M1("Test");
            }

            public virtual void M3()
            {
                Console.WriteLine("UserBaseLayer.A.M3");
                _B.M1(10);
                _C.M1("Test");
            }
        }

        public class B
        {
            public virtual void M1(int num)
            {
                Console.WriteLine("UserBaseLayer.B.M1:  num = " + num);
            }
        }

        public class C
        {
            public virtual void M1(string str)
            {
                Console.WriteLine("UserBaseLayer.C.M1:  str = " + str);
            }
        }
    }
}
