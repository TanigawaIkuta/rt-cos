﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;

namespace DebugApplication
{
    public class UserPartialLayer1 : PartialLayer
    {
        public UserPartialLayer1()
            : base("DebugApplication.UserBaseLayer", 15)
        {
        }

        public class A : ILayerActivationListener
        {
            public void M1()
            {
                Console.WriteLine("UserPartialLayer1.A.M1");
                UserBaseLayer.A obj = (UserBaseLayer.A)Layer.LayerdObjectAccessor.LayerdThis;
                obj.M2();
            }

            public void M2()
            {
                Console.WriteLine("UserPartialLayer1.A.M2");
                UserBaseLayer.A obj = (UserBaseLayer.A)Layer.LayerdObjectAccessor.LayerdThis;
                obj._B.M1(1);
                obj._C.M1("AAA");
            }

            void ILayerActivationListener.OnLayerActivation(Layer layer)
            {
                Console.WriteLine("ActivationListener A");
            }
        }

        public class B : ILayerDeactivationListener
        {
            public void M1(int num)
            {
                Console.WriteLine("UserPartialLayer1.B.M1:  num = " + num);
            }

            void ILayerDeactivationListener.OnLayerDeactivation(Layer layer)
            {
                Console.WriteLine("DeactivationListener B");
            }
        }

        public class C
        {
            public void M1(string str)
            {
                Console.WriteLine("UserPartialLayer1.C.M1:  str = " + str);
            }
        }
    }
}
