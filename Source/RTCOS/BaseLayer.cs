﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// ベースレイヤ
    /// </summary>
    public class BaseLayer : Layer
    {
        #region プロパティ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// ベースレイヤ
        /// </summary>
        public BaseLayer()
        {
        }

        #endregion
    }
}
