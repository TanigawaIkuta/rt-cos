﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;

namespace DebugApplication
{
    class LayersInitializer : ILayersInitializer
    {
        List<BaseLayer> _BaseLayers = new List<BaseLayer>();
        List<PartialLayer> _PartialLayers = new List<PartialLayer>();

        /// <summary>
        /// ベースレイヤ
        /// </summary>
        BaseLayer[] ILayersInitializer.BaseLayers { get { return _BaseLayers.ToArray(); } }

        /// <summary>
        /// パーシャルレイヤ
        /// </summary>
        PartialLayer[] ILayersInitializer.PartialLayers { get { return _PartialLayers.ToArray(); } }

        /// <summary>
        /// レイヤの生成
        /// </summary>
        void ILayersInitializer.CreateLayers()
        {
            _BaseLayers.Add(new UserBaseLayer());
            _PartialLayers.Add(new UserPartialLayer1());
            _PartialLayers.Add(new UserPartialLayer2());
        }
    }
}
