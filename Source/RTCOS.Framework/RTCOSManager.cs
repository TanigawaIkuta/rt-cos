﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RTCOS.Framework.Dispatch;
using RTCOS.Framework.Scheduling;

namespace RTCOS.Framework
{
    /// <summary>
    /// レイヤマネージャ
    /// </summary>
    public class RTCOSManager
    {
        #region フィールド
        /// <summary>
        /// レイヤリスト
        /// </summary>
        private LayerList _LayerList;

        /// <summary>
        /// コンテキストリスナ
        /// </summary>
        private ContextListener _ContextListener;

        /// <summary>
        /// レイヤスケジューラ
        /// </summary>
        private LayerScheduler _LayerScheduler;

        /// <summary>
        /// レイヤアクティベータ
        /// </summary>
        private LayerActivater _LayerActivater;

        /// <summary>
        /// レイヤ上オブジェクトクリエータ
        /// </summary>
        private LayeredObjectCreater _LayeredObjectCreater;

        /// <summary>
        /// メッセージディスパッチャ。
        /// </summary>
        private MessageDispatcher _MessageDispatcher;

        private LayerEventManager _LayerEventManager;

        /// <summary>
        /// 
        /// </summary>
        private ILayersInitializer _LayersInitializer;

        /// <summary>
        /// 
        /// </summary>
        private LayerSchedulingAlgorithm _LayerSchedulingAlgorithm;

        #endregion

        #region コンストラクタ
        /// <summary>
        /// 
        /// </summary>
        /// <param name="layersInitializer"></param>
        /// <param name="layerSchedulingAlgorithm"></param>
        public RTCOSManager(ILayersInitializer layersInitializer, LayerSchedulingAlgorithm layerSchedulingAlgorithm)
        {
            _LayersInitializer = layersInitializer;
            _LayerSchedulingAlgorithm = layerSchedulingAlgorithm;
        }

        #endregion

        #region メソッド
        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize()
        {
            // レイヤの初期化
            _LayersInitializer.CreateLayers();
            _LayerList = new LayerList(_LayersInitializer.BaseLayers.Length, _LayersInitializer.PartialLayers.Length);
            foreach (var layer in _LayersInitializer.BaseLayers)
                _LayerList.AddBaseLayer(layer);
            foreach (var layer in _LayersInitializer.PartialLayers)
                _LayerList.AddPartialLayer(layer);
            // レイヤイベントマネージャの初期化
            _LayerEventManager = new LayerEventManager();
            // コンテキストリスナの初期化
            _ContextListener = new ContextListener(_LayerList.BaseLayers.Count);
            // レイヤアクティベータの初期化
            _LayerActivater = new LayerActivater(_ContextListener, _LayerList);
            // メッセージディスパッチャの初期化
            _MessageDispatcher = new MessageDispatcher(_LayerList);
            // レイヤ上オブジェクトクリエータの初期化
            _LayeredObjectCreater = new LayeredObjectCreater(_MessageDispatcher, _LayerList, _LayerEventManager);

            // ユーザが用いるインターフェースの設定
            var layerList = typeof(Layer).GetProperty("LayerList", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            layerList.GetSetMethod(true).Invoke(null, new object[] { _LayerList });
            var layerActivater = typeof(Layer).GetProperty("LayerActivater", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            layerActivater.GetSetMethod(true).Invoke(null, new object[] { _LayerActivater });
            var layeredObjectCreater = typeof(Layer).GetProperty("LayeredObjectCreater", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            layeredObjectCreater.GetSetMethod(true).Invoke(null, new object[] { _LayeredObjectCreater });
            var layerdObjectAccessor = typeof(Layer).GetProperty("LayerdObjectAccessor", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            layerdObjectAccessor.GetSetMethod(true).Invoke(null, new object[] { _MessageDispatcher });

            // レイヤスケジューラ
            _LayerScheduler = new LayerScheduler(_ContextListener, _LayerList, _MessageDispatcher, _LayerEventManager, _LayerSchedulingAlgorithm);
            _LayerScheduler.Initialize();
            // レイヤ上オブジェクトクリエータの初期化
            _LayeredObjectCreater.Initialize();
            // メッセージディスパッチャの初期化
            _MessageDispatcher.Initialize();
        }

        /// <summary>
        /// ContextCSアプリケーションの実行。
        /// </summary>
        /// <param name="args">コマンドライン引数。</param>
        public void Execute(string[] args)
        {
            foreach (var layer in _LayerList.BaseLayers)
            {
                if (layer is ILayerEntryPoint)
                {
                    ((ILayerEntryPoint)layer).LayerMain(args);
                    break;
                }
            }
        }

        #endregion
    }
}
