﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS;

namespace DebugApplication
{
    public class LayerActivationLayer1 : PartialLayer
    {
        public LayerActivationLayer1()
            : base("DebugApplication.LayerActivationBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void Method0()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter0;
            }
            public void Method1()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
            }
            public void Method2()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
            }
            public void Method3()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
            }
            public void Method4()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
            }
        }
    }

    public class LayerActivationLayer2 : PartialLayer
    {
        public LayerActivationLayer2()
            : base("DebugApplication.LayerActivationBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void Method0()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter0;
            }
            public void Method1()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
            }
            public void Method2()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
            }
            public void Method3()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
            }
            public void Method4()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
            }
        }
    }

    public class LayerActivationLayer3 : PartialLayer
    {
        public LayerActivationLayer3()
            : base("DebugApplication.LayerActivationBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void Method0()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter0;
            }
            public void Method1()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
            }
            public void Method2()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
            }
            public void Method3()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
            }
            public void Method4()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
            }
        }
    }

    public class LayerActivationLayer4 : PartialLayer
    {
        public LayerActivationLayer4()
            : base("DebugApplication.LayerActivationBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void Method0()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter0;
            }
            public void Method1()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
            }
            public void Method2()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
            }
            public void Method3()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
            }
            public void Method4()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
            }
        }
    }

    public class LayerActivationLayer5 : PartialLayer
    {
        public LayerActivationLayer5()
            : base("DebugApplication.LayerActivationBaseLayer", 1)
        {
        }

        public class Benchmark
        {
            public void Method0()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter0;
            }
            public void Method1()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter1;
            }
            public void Method2()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter2;
            }
            public void Method3()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter3;
            }
            public void Method4()
            {
                ++((LayerActivationBaseLayer.Benchmark)Layer.LayerdObjectAccessor.LayerdThis)._Counter4;
            }
        }
    }
}
