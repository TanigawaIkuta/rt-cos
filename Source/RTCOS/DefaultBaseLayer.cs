﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// 明示的なレイヤ記述が無いクラスが属するレイヤ。
    /// </summary>
    public class DefaultBaseLayer : BaseLayer
    {
        #region 定数
        /// <summary>
        /// 
        /// </summary>
        public static readonly string DefaultBaseLayerName = @"DefaultBaseLayer";

        #endregion

        #region プロパティ
        public override string Name { get { return DefaultBaseLayerName; } }

        #endregion
    }
}
