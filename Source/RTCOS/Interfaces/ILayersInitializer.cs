﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤ初期化インターフェース。
    /// アプリケーション実行時にレイヤを生成する。
    /// </summary>
    public interface ILayersInitializer
    {
        #region プロパティ
        /// <summary>
        /// 生成したベースレイヤの集合。
        /// </summary>
        BaseLayer[] BaseLayers { get; }

        /// <summary>
        /// 生成したパーシャルレイヤの集合。
        /// </summary>
        PartialLayer[] PartialLayers { get; }

        #endregion

        #region メソッド
        /// <summary>
        /// アプリケーション実行時にレイヤを生成するメソッド。
        /// </summary>
        void CreateLayers();

        #endregion
    }
}
