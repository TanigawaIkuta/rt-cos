﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RTCOS.Framework;

namespace DebugApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new RTCOSApp(new LayersInitializer());
            //var app = new RTCOSApp(new MethodDispatchBaseLayer());
            //var app = new RTCOSApp(new LayerActivationBaseLayer());
            app.Execute(args);
        }
    }
}
