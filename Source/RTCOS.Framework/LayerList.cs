﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RTCOS.Framework
{
    /// <summary>
    /// レイヤリスト
    /// </summary>
    public class LayerList : ILayerList
    {
        #region フィールド
        /// <summary>
        /// アクティブレイヤフラグ
        /// </summary>
        private List<uint> _ActiveLayerFlag;

        //private ReaderWriterLockSlim _ReaderWriterLockSlim = new ReaderWriterLockSlim();

        #endregion

        #region プロパティ
        public override List<BaseLayer> BaseLayers { get { return _BaseLayers; } }
        private List<BaseLayer> _BaseLayers;

        public override List<PartialLayer> PartialLayers { get { return _PartialLayers; } }
        private List<PartialLayer> _PartialLayers;

        /*
        /// <summary>
        /// 
        /// </summary>
        public List<BaseLayer> BaseLayers { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public List<PartialLayer> PartialLayers { get; private set; }*/

        #endregion

        #region コンストラクタ
        public LayerList(int baseNum, int partialNum)
        {
            _BaseLayers = new List<BaseLayer>(baseNum);
            _PartialLayers = new List<PartialLayer>(partialNum);
            _ActiveLayerFlag = new List<uint>(partialNum + 1);
            for (int i = 0; i <= ((partialNum + 1) / (sizeof(uint) * 8)); ++i)
            {
                _ActiveLayerFlag.Add(0);
            }
            _ActiveLayerFlag[0] = 0x00000001;       // ベースレイヤの分
        }

        #endregion

        #region メソッド
        public void AddBaseLayer(BaseLayer layer)
        {
            //_ReaderWriterLockSlim.EnterWriteLock();
            try
            {
                BaseLayers.Add(layer);
            }
            finally
            {
                //_ReaderWriterLockSlim.ExitWriteLock();
            }
        }

        public void AddPartialLayer(PartialLayer layer)
        {
            //_ReaderWriterLockSlim.EnterWriteLock();
            try
            {
                if (((PartialLayers.Count + 1) / (sizeof(uint) * 8)) >= _ActiveLayerFlag.Count)
                {
                    _ActiveLayerFlag.Add(0);
                }
                PartialLayers.Add(layer);
            }
            finally
            {
                //_ReaderWriterLockSlim.ExitWriteLock();
            }
        }

        public bool CheckActiveFlag(int index)
        {
            //_ReaderWriterLockSlim.EnterReadLock();
            try
            {
                var div = index / (sizeof(uint) * 8);
                var modulo = index % (sizeof(uint) * 8);
                return ((_ActiveLayerFlag[div] & (0x01 << modulo)) != 0);
            }
            finally
            {
                //_ReaderWriterLockSlim.ExitReadLock();
            }
        }

        public void SetActiveFlag(int index)
        {
            //_ReaderWriterLockSlim.EnterWriteLock();
            try
            {
                var div = index / (sizeof(uint) * 8);
                var modulo = index % (sizeof(uint) * 8);
                _ActiveLayerFlag[div] |= (uint)(0x01 << modulo);
            }
            finally
            {
                //_ReaderWriterLockSlim.ExitWriteLock();
            }
        }

        public void RemoveActiveFlag(int index)
        {
            //_ReaderWriterLockSlim.EnterWriteLock();
            try
            {
                var div = index / (sizeof(uint) * 8);
                var modulo = index % (sizeof(uint) * 8);
                _ActiveLayerFlag[div] ^= (uint)(0x01 << modulo);
            }
            finally
            {
                //_ReaderWriterLockSlim.ExitWriteLock();
            }
        }

        #endregion
    }
}
