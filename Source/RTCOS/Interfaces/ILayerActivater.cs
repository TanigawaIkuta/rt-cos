﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// レイヤアクティベータのインターフェース。
    /// レイヤアクティベーションに関する命令を持つ。
    /// </summary>
    public abstract class ILayerActivater
    {
        #region メソッド
        /// <summary>
        /// レイヤアクティベート。
        /// </summary>
        /// <param name="layerName">アクティベートするレイヤ。</param>
        public abstract void ActivateLayer(string layerName);

        /// <summary>
        /// レイヤディアクティベート。
        /// </summary>
        /// <param name="layerName">ディアクティベートするレイヤ。</param>
        public abstract void DeactivateLayer(string layerName);

        /// <summary>
        /// レイヤクリティカルセクションに入ることを通知する。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        public abstract void EnterLayerCriticalSection(string baseLayerName);

        /// <summary>
        /// レイヤクリティカルセクションから出ることを通知する。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        public abstract void ExitLayerCriticalSection(string baseLayerName);

        /// <summary>
        /// レイヤ操作の完了待ち。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        public abstract void WaitLayerOperationCompletion(string baseLayerName);

        #endregion
    }

    /*
    /// <summary>
    /// レイヤアクティベータのインターフェース。
    /// レイヤアクティベーションに関する命令を持つ。
    /// </summary>
    public interface ILayerActivater
    {
        #region メソッド
        /// <summary>
        /// レイヤアクティベート。
        /// </summary>
        /// <param name="layerName">アクティベートするレイヤ。</param>
        void ActivateLayer(string layerName);

        /// <summary>
        /// レイヤディアクティベート。
        /// </summary>
        /// <param name="layerName">ディアクティベートするレイヤ。</param>
        void DeactivateLayer(string layerName);

        /// <summary>
        /// レイヤクリティカルセクションに入ることを通知する。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        void EnterLayerCriticalSection(string baseLayerName);

        /// <summary>
        /// レイヤクリティカルセクションから出ることを通知する。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        void ExitLayerCriticalSection(string baseLayerName);

        /// <summary>
        /// レイヤ操作の完了待ち。
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名。</param>
        void WaitLayerOperationCompletion(string baseLayerName);

        #endregion
    }*/
}
