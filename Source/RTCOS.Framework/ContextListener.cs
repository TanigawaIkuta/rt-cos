﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RTCOS.Framework
{
    /// <summary>
    /// コンテキストリスナ
    /// </summary>
    internal class ContextListener
    {
        #region フィールド
        /// <summary>
        /// 
        /// </summary>
        private List<ReaderWriterLockSlim> _ReaderWriterLockList = new List<ReaderWriterLockSlim>();

        internal bool _CompletionFlag = true;

        #endregion

        #region プロパティ
        public Queue<LayerOperation> LayerOperationQueue { get; private set; }

        #endregion

        #region コンストラクタ
        public ContextListener(int baseLayerNum)
        {
            LayerOperationQueue = new Queue<LayerOperation>();
            for (int i = 0; i < baseLayerNum; ++i)
            {
                _ReaderWriterLockList.Add(new ReaderWriterLockSlim());
            }
        }

        #endregion

        #region メソッド
        /// <summary>
        /// レイヤクリティカルセクションに入ることを通知する
        /// </summary>
        /// <param name="baseLayerID">ベースレイヤID</param>
        public void EnterLayerCriticalSection(int baseLayerID)
        {
            CheckReaderWriterLockCount(baseLayerID);
            // ここ後で直す!!
            if (!_ReaderWriterLockList[baseLayerID].IsReadLockHeld)
            {
                _ReaderWriterLockList[baseLayerID].EnterReadLock();
            }
        }

        /// <summary>
        /// レイヤクリティカルセクションから出ることを通知する
        /// </summary>
        /// <param name="baseLayerID">ベースレイヤID</param>
        public void ExitLayerCriticalSection(int baseLayerID)
        {
            CheckReaderWriterLockCount(baseLayerID);
            _ReaderWriterLockList[baseLayerID].ExitReadLock();
        }

        /// <summary>
        /// レイヤ操作に入ることを通知する
        /// </summary>
        /// <param name="baseLayerID">ベースレイヤID</param>
        public void EnterLayerOperating(int baseLayerID)
        {
            CheckReaderWriterLockCount(baseLayerID);
            _ReaderWriterLockList[baseLayerID].EnterWriteLock();
        }

        /// <summary>
        /// レイヤ操作から出ることを通知する
        /// </summary>
        /// <param name="baseLayerID">ベースレイヤID</param>
        public void ExitLayerOperating(int baseLayerID)
        {
            CheckReaderWriterLockCount(baseLayerID);
            _ReaderWriterLockList[baseLayerID].ExitWriteLock();
        }

        /// <summary>
        /// レイヤ操作の完了待ち
        /// </summary>
        /// <param name="baseLayerName">ベースレイヤ名</param>
        public void WaitLayerOperationCompletion(int baseLayerID)
        {
            while (!_CompletionFlag)
            {
                Thread.Sleep(1);
            }
        }

        private void CheckReaderWriterLockCount(int baseLayerID)
        {
            var count = _ReaderWriterLockList.Count;
            if (count <= baseLayerID)
            {
                for (int i = count; i <= baseLayerID; ++i)
                {
                    _ReaderWriterLockList.Add(new ReaderWriterLockSlim());
                }
            }
        }

        #endregion
    }

    internal struct LayerOperation
    {
        #region プロパティ
        public LayerOperationKind Kind { get { return _Kind; } }
        private LayerOperationKind _Kind;

        public string LayerName { get { return _LayerName; } }
        private string _LayerName;

        #endregion

        #region コンストラクタ
        public LayerOperation(LayerOperationKind kind, string name)
        {
            _Kind = kind;
            _LayerName = name;
        }

        #endregion
    }

    internal enum LayerOperationKind
    {
        None,
        Activate,
        Deactivate,
    }
}
