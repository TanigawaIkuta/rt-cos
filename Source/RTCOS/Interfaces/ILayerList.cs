﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTCOS
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ILayerList
    {
        /// <summary>
        /// 
        /// </summary>
        public abstract List<BaseLayer> BaseLayers { get; }

        /// <summary>
        /// 
        /// </summary>
        public abstract List<PartialLayer> PartialLayers { get; }
    }

    /*
    /// <summary>
    /// 
    /// </summary>
    public interface ILayerList
    {
        /// <summary>
        /// 
        /// </summary>
        List<BaseLayer> BaseLayers { get; }

        /// <summary>
        /// 
        /// </summary>
        List<PartialLayer> PartialLayers { get; }
    }*/
}
